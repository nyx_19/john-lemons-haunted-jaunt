﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float jumppadHeight = 700f;    //for jumping
    public float jumpHeight = 300f;
    public bool isGrounded;           //for jumping
    public bool ShouldJump;
    public AudioSource jumppad;
    public AudioSource jump;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    // Start is called before the first frame update
    void Start ()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);  //it tabbed it over when i finished to be flush against approximately and idk if that will be an issue later we will see
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if(!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f); // might need to indent before time.deltatime to be exact not sure
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
 
    void Update() //trying to get in jumping
    {
        if (isGrounded)
        {
            if(ShouldJump)
            {
                ShouldJump = false;
                Debug.Log("hi");
                m_Rigidbody.AddForce(Vector3.up * jumppadHeight);
                jumppad.Play();
            }
        }
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true)
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true)
        {
            m_Rigidbody.AddForce(Vector3.up * jumpHeight);
            jump.Play();
        }
    }


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }
    
    void OnAnimatorMove ()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void OnTriggerEnter(Collider hit)
    {
        Debug.Log("hi");
        switch(hit.gameObject.tag)
        {
            case "JumpPad":
                ShouldJump = true;
                break;
        }
    }
}
